// Translate Data Models into codes

// Users Collection 

{
	"_id": "user001",
	"firstName": "John",
	"lastName": "Smith",
	"email": "johnsmith@mail.com",
	"password": "johnsmith123",
	"isAdmin": true,
	"mobileNumber": "09123456789",
	"dateTimeRegistered": "2022-06-10T15:00:00.00Z" //dateTime datatype
}
{
	"_id": "user002",
	"firstName": "Jane",
	"lastName": "Doe",
	"email": "janedoe@mail.com",
	"password": "janedoe123",
	"isAdmin": false,
	"mobileNumber": "0912123123",
	"dateTimeRegistered": "2022-06-10T15:00:00.00Z" //dateTime datatype
}
{
	"_id": "user003",
	"firstName": "Pedro",
	"lastName": "Pendoko",
	"email": "pedropendoko@mail.com",
	"password": "pedro123",
	"isAdmin": false,
	"mobileNumber": "0912000001",
	"dateTimeRegistered": "2022-06-10T15:00:00.00Z" //dateTime datatype
}

// Orders Collection
{
	"_id": "order001",
	"userID": user003,
	"transactionDate": "2022-06-08",
	"isPaid": true,
	"total": 43000
	
}
{
	"_id": "order002",
	"userID": user002,
	"transactionDate": "2022-06-10",
	"isPaid": false,
	"total": 40000
	
}

// Order Products Collection
{
	"_id": "orderproducts001",
	"orderID": order001,
	"productID": ["product001","product002"],
	"quantity": [2,1],
	"price": [20000,3000],
	"subtotal": [40000,3000]
}
{
	"_id": "orderproducts002",
	"orderID": order002,
	"productID": ["product003"],
	"quantity": [1],
	"price": [17000],
	"subtotal": 17000
}
{
	"_id": "orderproducts003",
	"orderID": order002,
	"productID": ["product004","products005"],
	"quantity": [1,3],
	"price": [10000,1000],
	"subtotal": [10000,3000]
}

// Products Collection
{
	"_id": "product001",
	"name": "Xiaomi Pad 5",
	"description": "Xiaomi Pad 5 comes equipped with large 11 screen, slim, stylish design,Qualcomm® Snapdragon™ 860",
	"price": 20000,
	"stocks": 13, //less 2 stocks
	"isActive": true,
	"sku": "sku00001"
}
{
	"_id": "product002",
	"name": "Xiaomi Stylus Pen",
	"description": "Xiaomi Smart Pen",
	"price": 3000,
	"stocks": 9, //less 1 stock
	"isActive": true,
	"sku": "sku00002"
}
{
	"_id": "product003",
	"name": "Xiaomi Redmi Note 10 Pro 5G",
	"description": "Xiaomi Redmi Note 11 Pro PlusNetwork mode: dual card dual standby",
	"price": 17000,
	"stocks": 9, //less 1 stock
	"isActive": true,
	"sku": "sku00003"
}

{
	"_id": "product004",
	"name": "Nokia 3310 Collector's Edition",
	"description": "Most Durable Phone ever made.",
	"price": 10000,
	"stocks": 4, // less 1 stock
	"isActive": true,
	"sku": "sku00004"
}
{
	"_id": "product005",
	"name": "Nokia 3310 Red Case",
	"description": "Case for Nokia 3310",
	"price": 1000,
	"stocks": 2, // less 3 stocks
	"isActive": true,
	"sku": "sku00005"
}